# frozen_string_literal: true

describe docker.containers do
  its('names') { should_not include 'traefik' }
  its('images') { should_not include 'traefik:v2.5' }
end
