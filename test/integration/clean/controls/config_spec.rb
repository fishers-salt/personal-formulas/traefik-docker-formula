# frozen_string_literal: true

control 'traefik-docker-config-clean-global-config-absent' do
  title 'should not exist'

  describe file('/srv/docker/traefik/config/traefik.yaml') do
    it { should_not exist }
  end
end

control 'traefik-config-clean-docker-traefik-letsencrypt-directory-absent' do
  title 'should not exist'

  describe directory('/srv/docker/traefik/letsencrypt') do
    it { should_not exist }
  end
end

control 'traefik-config-clean-docker-traefik-config-directory-absent' do
  title 'should not exist'

  describe directory('/srv/docker/traefik/config') do
    it { should_not exist }
  end
end
