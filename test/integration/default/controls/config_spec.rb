# frozen_string_literal: true

control 'traefik-config-file-docker-traefik-config-directory-managed' do
  title 'should exist'

  describe directory('/srv/docker/traefik/config') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0755' }
  end
end

control 'traefik-config-file-docker-traefik-letsencrypt-directory-managed' do
  title 'should exist'

  describe directory('/srv/docker/traefik/letsencrypt') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0755' }
  end
end

control 'traefik-docker-config-file-global-config-managed' do
  title 'should match desired lines'

  describe file('/srv/docker/traefik/config/traefik.yaml') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('endpoint: "unix:///var/run/docker.sock"') }
    its('content') { should include('caServer: https://acme-staging-v02') }
  end
end
