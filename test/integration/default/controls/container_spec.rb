# frozen_string_literal: true

describe docker_container(name: 'traefik') do
  it { should exist }
  it { should be_running }
  its('image') { should eq 'traefik:v2.5' }
  its('ports') do
    should eq '0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp, 0.0.0.0:8080->8080/tcp'
  end
  it { should have_volume('/etc/traefik', '/srv/docker/traefik/config') }
  it { should have_volume('/letsencrypt', '/srv/docker/traefik/certificates') }
  it { should have_volume('/var/run/docker.sock', '/var/run/docker.sock') }
end
