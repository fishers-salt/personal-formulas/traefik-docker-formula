# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as traefik_docker with context %}

{%- set mountpoint = traefik_docker.docker.mountpoint %}

traefik-docker-config-clean-global-config-absent:
  file.absent:
    - name: {{ mountpoint }}/docker/traefik/config/traefik.yaml

{%- for dir in [
  'docker/traefik/config',
  'docker/traefik/letsencrypt'
  ]
%}
traefik-docker-config-clean-{{ dir|replace('/', '_') }}-directory-absent:
  file.absent:
    - name: {{ mountpoint }}/{{ dir }}
{% endfor %}
