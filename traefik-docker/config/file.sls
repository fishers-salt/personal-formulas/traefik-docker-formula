# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as traefik_docker with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set mountpoint = traefik_docker.docker.mountpoint %}
{%- set letsencrypt_email = traefik_docker.get('letsencrypt_email', 'test@example.com') %}
{%- set ca_server = traefik_docker.ca_server %}

{%- for dir in [
  'docker/traefik/config',
  'docker/traefik/letsencrypt'
  ]
%}

traefik-docker-config-file-{{ dir|replace('/', '_') }}-directory-managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: root
    - group: root
    - dir_mode: '0755'
    - file_mode: '0644'
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}

traefik-docker-config-file-global-config-managed:
  file.managed:
    - name: {{ mountpoint }}/docker/traefik/config/traefik.yaml
    - source: {{ files_switch(['traefik.yaml.tmpl'],
                              lookup='traefik-config-file-traefik-global-config-managed',
                 )
              }}
    - user: root
    - group: root
    - mode: '0644'
    - makedirs: True
    - template: jinja
    - context:
        letsencrypt_email: {{ letsencrypt_email }}
        ca_server: {{ ca_server }}
