# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as traefik_docker with context %}

include:
  - {{ sls_config_clean }}

{%- set name = traefik_docker.container.name %}

traefik-container-clean-container-absent:
  docker_container.absent:
    - name: {{ name }}
    - force: True

traefik-docker-clean-image-absent:
  docker_image.absent:
    - name: {{ name }}
    - require:
      - traefik-container-clean-container-absent
