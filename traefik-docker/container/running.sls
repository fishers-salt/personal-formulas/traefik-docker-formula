# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as traefik_docker with context %}
{%- set sls_config_file = tplroot ~ '.config.file' %}

include:
  - {{ sls_config_file }}

{%- set do_auth_token = traefik_docker.get('do_auth_token', '12345') %}

{%- set name = traefik_docker.container.name %}
{%- set image = traefik_docker.container.image %}
{%- set tag = traefik_docker.container.tag %}
{%- set mountpoint = traefik_docker.docker.mountpoint %}
traefik-docker-container-running-image-present:
  docker_image.present:
    - name: {{ name }}
    - tag: {{ tag }}
    - force: True

traefik-docker-container-running-container-managed:
  docker_container.running:
    - name: {{ name }}
    - image: {{ image }}:{{ tag }}
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/traefik/config:/etc/traefik/
      - {{ mountpoint }}/docker/traefik/certificates:/letsencrypt
      - /var/run/docker.sock:/var/run/docker.sock
    - port_bindings:
      - 8080:8080
      - 80:80
      - 443:443
    - dns: {{ traefik_docker.dns_servers }}
    - watch:
      - sls: {{ sls_config_file }}
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
      - DO_AUTH_TOKEN: {{ do_auth_token }}
